var map = L.map('main_map').setView([36.5192975,-6.2907148], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/* L.marker([36.5192975,-6.2806148]).addTo(map);
L.marker([36.506,-6.2706148]).addTo(map); */

//Hace una solicitud asincronico a una web en el formato especificado 
$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result){  //Si tiene exito se agregan las bicicletas al mapa
    console.log(result);
    result.bicicleta.forEach(function(bici){
      L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
    });
  }
})
