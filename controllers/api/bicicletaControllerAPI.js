var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, res){
  res.status(200).json({
    bicicleta: Bicicleta.allBicis
  });
}

exports.bicicleta_create = function(req, res){
  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];

  Bicicleta.add(bici);

  res.status(200).json({
    bicicleta: bici
  });
}

exports.bicicleta_delete = function(req, res){
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
}

exports.bicicleta_update = function(req, res){
  var biciId = req.params.id

  var bici_updated = Bicicleta.findById(biciId)
  bici_updated.id = req.body.id;
  bici_updated.color = req.body.color;
  bici_updated.modelo = req.body.modelo;
  bici_updated.ubicacion = [req.body.lat, req.body.lng];

  res.status(200).send({ bicicleta: bici_updated })
}
