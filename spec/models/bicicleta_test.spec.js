var Bicicleta = require('../../models/bicicleta');


// Tests locales

// Ejecuta esta accion antes de cada test, reduciendo el codigo repetitivo
beforeEach(() => { Bicicleta.allBicis = []; });

// Recibe una descripcion y la funcion que ejecuta la prueba
describe('Bicicleta.allBicis', () => {
  it('Comienza vacio', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe('Bicicleta.add', () => {
  it('Se agrega una bicicleta', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      var bici = new Bicicleta(1, 'rojo', 'urbana', [36.5192975,-6.2806148]);
      Bicicleta.add(bici);
      expect(Bicicleta.allBicis.length).toBe(1);
      expect(Bicicleta.allBicis[0]).toBe(bici);
  });
});

describe('Bicicleta.findById', () => {
  it('Retorna una bicicleta con id 1', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      var bici1 = new Bicicleta(1, 'rojo', 'urbana', [36.5192975,-6.2806148]);
      var bici2 = new Bicicleta(2, 'blanca', 'montaña', [36.506,-6.2706148]);
      Bicicleta.add(bici1);
      Bicicleta.add(bici2);
      
      var targetBici = Bicicleta.findById(1);
      expect(targetBici.id).toBe(1);
      expect(targetBici.color).toBe(bici1.color);
      expect(targetBici.modelo).toBe(bici1.modelo);
  })
});

describe('Bicicleta.removeById', () => {
  it('Borra el elemento con id 1', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      var bici = new Bicicleta(1, 'rojo', 'urbana', [36.5192975,-6.2806148]);
      Bicicleta.add(bici);
      Bicicleta.removeById(1);
      expect(Bicicleta.allBicis.length).toBe(0);
  });
});